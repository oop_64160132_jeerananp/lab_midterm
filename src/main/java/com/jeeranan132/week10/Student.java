package com.jeeranan132.week10;

import java.time.LocalDate;

public class Student {
    private String name;
    private int ID;
    private String faculty;
    private LocalDate time;
    private LocalDate returnTime;

    public Student(String name, int ID, String faculty) {
        this.name = name;
        this.ID = ID;
        this.faculty = faculty;
        this.time = null;
        this.returnTime = null;
    }

    public String getName() {
        return name;
    }

    public int getID() {
        return ID;
    }

    public String getFaculty() {
        return faculty;
    }

    public LocalDate getTime() {
        time = LocalDate.now();
        return time;
    }

    public LocalDate getReturnBookTime() {
        returnTime = time.plusDays(14);
        return returnTime;
    }

}
