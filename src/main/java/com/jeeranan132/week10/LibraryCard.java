package com.jeeranan132.week10;

public class LibraryCard {
    private int borrowCout;
    private Student owner;
    private int returnCout;

    public LibraryCard() {
        this.owner = null;
        this.borrowCout = 0;
        this.returnCout = 0;
    }

    public void checkOut(int numOfBook) {
        borrowCout = borrowCout + numOfBook;
    }

    public int getCheckOut() {
        return borrowCout;
    }

    public void checkIn(int returnCoutBook) {
        returnCout = borrowCout - returnCoutBook;
    }

    public int getCheckIn() {
        return returnCout;
    }

    public String getNameStudent() {
        return owner.getName();
    }

    public void setOwner(Student student) {
        owner = student;
    }

    public String toString() {
        return ("===============================") + "\n" +
                ("     ยินดีต้องรับสู่ Buu Library     ") + "\n" +
                "Owner Name : " + owner.getName() + "\n" + "ID : " + owner.getID() + "\n" + "Email : "
                + owner.getID() + "@go.buu.ac.th" + "\n" + "Faculty of : " + owner.getFaculty() + "\n" + "\n"
                + "จำนวนหนังสือที่ยืม : " + borrowCout + " เล่ม" + "\n" + "เหลือหนังสือที่ต้องคืน : " + returnCout
                + " เล่ม" + "\n" + "วันที่ยืมหนังสือ : "
                + owner.getTime() + "\n"
                + "กรุณาส่งคืนภายในวันที่ : " + owner.getReturnBookTime() + "\n" +
                ("         ขอบคุณที่ใช้บริการ        ") + "\n" +
                ("===============================");

    }
}
