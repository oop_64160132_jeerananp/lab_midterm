package com.jeeranan132.week10;

public class LibraryApp {
    public static void main(String[] args) {
        Student student1 = new Student("Jeeranan Pongpitak", 64160132, "Infomatics");
        Student student2 = new Student("Tanathor Keama", 64160789, "Infomatics");
        Student student3 = new Student("Sirinda rakchean", 64160499, "Infomatics");
        student1.getName();
        student1.getID();
        student1.getFaculty();
        student1.getTime();

        student2.getName();
        student2.getID();
        student2.getFaculty();
        student2.getTime();

        student3.getName();
        student3.getID();
        student3.getFaculty();
        student3.getTime();

        LibraryCard lCard1 = new LibraryCard();
        LibraryCard lCard2 = new LibraryCard();
        LibraryCard lCard3 = new LibraryCard();
        lCard1.setOwner(student1);
        lCard1.checkOut(2);
        lCard1.checkIn(1);

        lCard2.setOwner(student2);
        lCard2.checkOut(10);
        lCard2.checkIn(3);

        lCard3.setOwner(student3);
        lCard3.checkOut(6);
        lCard3.checkIn(2);

        System.out.println(lCard1.toString() + "\n");

        System.out.println(lCard2.toString() + "\n");

        System.out.println(lCard3.toString() + "\n");
    }
}
